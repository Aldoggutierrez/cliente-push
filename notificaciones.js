async function requestPermission() {
    const resultado = await Notification.requestPermission();
    if (resultado === 'denied') {
        throw Error("se nego el permiso para las notificaciones");
    }
    if (resultado === 'granted') {
    console.info("se otorgo el permiso para las notificaciones");
    }
}

async function registerServiceWorker() {
    const serviceWorker = await navigator.serviceWorker.register("/service-worker.js")
    if (serviceWorker) {
        console.log("se registro el service worker");
    }
    else {
        throw Error("no se pudo registrar el service worker");
    }
}

async function subscribeToPush() {
 
    const registro = await navigator.serviceWorker.getRegistration();

    if (registro) {
        console.log("si hay registro de service worker");
        
        const suscripcion = await registro.pushManager.getSubscription();

        if (!suscripcion) {
            console.log("no hay suscripcion de push");
            
            // const key = await fetch("https://example.com/vapid_key");
            // const keyData = await key.text();

            // const sub = await registro.pushManager.subscribe({
            //     applicationServerKey: keyData,
            //     userVisibleOnly: true
            // });

            // await fetch("https://example.com/push_subscribe", {
            //     method: "POST",
            //     headers: {"Content-Type": "application/json"},
            //     body: JSON.stringify({
            //         endpoint: sub.endpoint,
            //         expirationTime: sub.expirationTime,
            //         keys: sub.toJSON().keys
            //     })
            // });

        }
        else {
            console.log(suscripcion);
        }
    } else {
        
    }
}

if (!('serviceWorker' in navigator)) {
    throw Error("el navegador no soporta service workers");
}
if (!("Notification" in window)) {
    throw Error("el navegador no soporta notificaciones");
}
if (!('PushManager' in window)) {
    throw Error("el navegador no soporta notificaciones push");
}

requestPermission();
registerServiceWorker();
subscribeToPush();


